package com.bi.service.connection;

import com.bi.model.connection.DbTableInfo;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface DbTableInfoService {

    public DbTableInfo save(DbTableInfo dbTableInfo);
    public Optional<DbTableInfo> findById(Long id);
    public List<DbTableInfo> findAll();
    public List<DbTableInfo> findAll(Sort sort);
}
