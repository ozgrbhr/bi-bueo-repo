package com.bi.service.connection.impl;

import com.bi.model.connection.DbSchemaInfo;
import com.bi.repository.connection.DbSchemaInfoRepository;
import com.bi.service.connection.DbSchemaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DbSchemaInfoServiceImpl implements DbSchemaInfoService {

    @Autowired
    private DbSchemaInfoRepository dbSchemaInfoRepository;

    @Override
    public DbSchemaInfo save(DbSchemaInfo dbSchemaInfo) {
        return this.dbSchemaInfoRepository.save(dbSchemaInfo);
    }

    @Override
    public Optional<DbSchemaInfo> findById(Long id) {
        return this.dbSchemaInfoRepository.findById(id);
    }

    @Override
    public List<DbSchemaInfo> findAll() {
        return this.dbSchemaInfoRepository.findAll();
    }

    @Override
    public List<DbSchemaInfo> findAll(Sort sort) {
        return this.dbSchemaInfoRepository.findAll(sort);
    }
}
