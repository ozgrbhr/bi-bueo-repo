package com.bi.service.elasticsearch;

import org.elasticsearch.client.RestHighLevelClient;

public interface ElasticsearchService {
    public RestHighLevelClient getClient();
    public void closeClient();
    public boolean createIndex(String indexName);
    public boolean deleteIndex(String indexName);
    public boolean openIndex(String indexName);
    public boolean closeIndex(String indexName);
}
