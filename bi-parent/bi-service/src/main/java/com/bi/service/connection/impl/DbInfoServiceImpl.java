package com.bi.service.connection.impl;

import com.bi.model.connection.DbInfo;
import com.bi.repository.connection.DbInfoRepository;
import com.bi.service.connection.DbInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DbInfoServiceImpl implements DbInfoService {

    @Autowired
    private DbInfoRepository dbInfoRepository;

    @Override
    public DbInfo save(DbInfo dbInfo) {
        return this.dbInfoRepository.save(dbInfo);
    }

    @Override
    public Optional<DbInfo> findById(Long id) {
        return this.dbInfoRepository.findById(id);
    }

    @Override
    public List<DbInfo> findAll() {
        return this.dbInfoRepository.findAll();
    }

    @Override
    public List<DbInfo> findAll(Sort sort) {
        return this.dbInfoRepository.findAll(sort);
    }
}
