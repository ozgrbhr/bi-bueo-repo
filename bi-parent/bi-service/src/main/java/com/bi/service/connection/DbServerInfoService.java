package com.bi.service.connection;

import com.bi.model.connection.DbServerInfo;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface DbServerInfoService {

    public DbServerInfo save(DbServerInfo dbServerInfo);
    public Optional<DbServerInfo> findById(Long id);
    public List<DbServerInfo> findAll();
    public List<DbServerInfo> findAll(Sort sort);
}
