package com.bi.service.connection.impl;

import com.bi.model.connection.OdbcDriver;
import com.bi.repository.connection.OdbcDriverRepository;
import com.bi.service.connection.OdbcDriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OdbcDriverServiceImpl implements OdbcDriverService {

    @Autowired
    private OdbcDriverRepository odbcDriverRepository;

    @Override
    public OdbcDriver save(OdbcDriver odbcDriver) {
        return odbcDriverRepository.save(odbcDriver);
    }

    @Override
    public Optional<OdbcDriver> findById(Long id) {
        return odbcDriverRepository.findById(id);
    }

    @Override
    public List<OdbcDriver> findAll() {
        return odbcDriverRepository.findAll();
    }

    @Override
    public List<OdbcDriver> findAll(Sort sort) {
        return odbcDriverRepository.findAll(sort);
    }
}
