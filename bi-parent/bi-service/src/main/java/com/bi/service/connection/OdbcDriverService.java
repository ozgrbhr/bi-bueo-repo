package com.bi.service.connection;

import com.bi.model.connection.OdbcDriver;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface OdbcDriverService {

    public OdbcDriver save(OdbcDriver odbcDriver);
    public Optional<OdbcDriver> findById(Long id);
    public List<OdbcDriver> findAll();
    public List<OdbcDriver> findAll(Sort sort);

}
