package com.bi.service.connection;

import com.bi.model.connection.DbInfo;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface DbInfoService {

    public DbInfo save(DbInfo dbInfo);
    public Optional<DbInfo> findById(Long id);
    public List<DbInfo> findAll();
    public List<DbInfo> findAll(Sort sort);
}
