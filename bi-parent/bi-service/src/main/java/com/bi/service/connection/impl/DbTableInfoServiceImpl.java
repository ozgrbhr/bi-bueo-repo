package com.bi.service.connection.impl;

import com.bi.model.connection.DbTableInfo;
import com.bi.repository.connection.DbTableInfoRepository;
import com.bi.service.connection.DbTableInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DbTableInfoServiceImpl implements DbTableInfoService {

    @Autowired
    private DbTableInfoRepository dbTableInfoRepository;

    @Override
    public DbTableInfo save(DbTableInfo dbTableInfo) {
        return this.dbTableInfoRepository.save(dbTableInfo);
    }

    @Override
    public Optional<DbTableInfo> findById(Long id) {
        return this.dbTableInfoRepository.findById(id);
    }

    @Override
    public List<DbTableInfo> findAll() {
        return this.dbTableInfoRepository.findAll();
    }

    @Override
    public List<DbTableInfo> findAll(Sort sort) {
        return this.dbTableInfoRepository.findAll(sort);
    }
}
