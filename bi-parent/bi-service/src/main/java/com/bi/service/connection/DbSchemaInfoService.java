package com.bi.service.connection;

import com.bi.model.connection.DbSchemaInfo;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface DbSchemaInfoService {

    public DbSchemaInfo save(DbSchemaInfo dbSchemaInfo);
    public Optional<DbSchemaInfo> findById(Long id);
    public List<DbSchemaInfo> findAll();
    public List<DbSchemaInfo> findAll(Sort sort);
}
