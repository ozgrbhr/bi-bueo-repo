package com.bi.service.elasticsearch.impl;

import com.bi.service.elasticsearch.ElasticsearchService;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.close.CloseIndexRequest;
import org.elasticsearch.action.admin.indices.close.CloseIndexResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.open.OpenIndexRequest;
import org.elasticsearch.action.admin.indices.open.OpenIndexResponse;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ElasticsearchServiceImpl implements ElasticsearchService{

    private RestHighLevelClient client = null;

    @Override
    public RestHighLevelClient getClient() {

        if(client == null) {
            client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
        }

        return client;
    }

    @Override
    public void closeClient() {
        try {
            if(client != null)
                client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean createIndex(String indexName) {
        CreateIndexRequest request = new CreateIndexRequest(indexName);

        // Each index created can have specific settings associated with it.
        // Shard'lar store edilmiş olan document'ların tutulduğu küçük birimlerdir.
        // Her bir shard aslında birer lucene instance'ına tekabül eder.
        // bir Index'in kaç adet shard'a sahip olacağını özellikle belirtmediysek bizler için 5 adet shard yaratılır.
        //Bir index yarattığımızı ve 1000 adet document store ettiğimizi düşünelim.
        // Bu durumda elasticsearch yaratılan 5 shard'a eşit bir şekilde bu document'ları distribute edecektir.
        //  request'i alan elasticsearch engine paralel olacak şekilde tüm shard'lar üzerinde bu searching işlemini gerçekleştirir
        // ve tüm shard'lar cevap verdikten sonra hepsini concat ederek bize sunar.
        // Her bir shard'ın birer lucene instance'ı olmasının faydasını bu noktada görüyoruz.
        // Çünkü aynı node üzerinde dahi birden fazla shard varsa kendi aralarında paralel olarak çalıştırılabilirler.
        request.settings(Settings.builder()
                .put("index.number_of_shards", 5)
//                .put("index.number_of_replicas", 2) // Simdilik replica yok
        );

        // Timeout to wait for the all the nodes to acknowledge the index creation as a TimeValue
        // degistirebiliriz sureyi
        request.timeout(TimeValue.timeValueMinutes(2));

        // Timeout to connect to the master node as a TimeValue
        // degistirebiliriz sureyi
        request.masterNodeTimeout(TimeValue.timeValueMinutes(1));

        // The number of active shard copies to wait for before the create index API returns a response, as an int.
//        request.waitForActiveShards(2);
        //The number of active shard copies to wait for before the create index API returns a response, as an ActiveShardCount.
        request.waitForActiveShards(ActiveShardCount.DEFAULT);


        boolean acknowledged = false;
        boolean shardsAcknowledged = false;
        try {
            // Synchronous Execution
            CreateIndexResponse createIndexResponse = getClient().indices().create(request);
            //Indicates whether all of the nodes have acknowledged the request
            acknowledged = createIndexResponse.isAcknowledged();
            // Indicates whether the requisite number of shard copies were started for each shard in the index before timing out
            shardsAcknowledged = createIndexResponse.isShardsAcked();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return acknowledged && shardsAcknowledged; // ikisi de true'ysa problemsiz Index oluşturuldu.
    }

    @Override
    public boolean deleteIndex(String indexName) {
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);

        // Timeout to wait for the all the nodes to acknowledge the index deletion as a TimeValue
        // degistirebiliriz sureyi
        request.timeout(TimeValue.timeValueMinutes(2));

        // Timeout to connect to the master node as a TimeValue
        // degistirebiliriz sureyi
        request.masterNodeTimeout(TimeValue.timeValueMinutes(1));

        // Setting IndicesOptions controls how unavailable indices are resolved and how wildcard expressions are expanded
        request.indicesOptions(IndicesOptions.lenientExpandOpen());

        boolean acknowledged = false;
        try {
            DeleteIndexResponse deleteIndexResponse = getClient().indices().delete(request);
            //Indicates whether all of the nodes have acknowledged the request
            acknowledged = deleteIndexResponse.isAcknowledged();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return acknowledged;
    }

    @Override
    public boolean openIndex(String indexName) {
        // The index to open
        OpenIndexRequest request = new OpenIndexRequest(indexName);

        // Timeout to wait for the all the nodes to acknowledge the index is opened as a TimeValue
        // degistirebiliriz sureyi
        request.timeout(TimeValue.timeValueMinutes(2));

        // Timeout to connect to the master node as a TimeValue
        // degistirebiliriz sureyi
        request.masterNodeTimeout(TimeValue.timeValueMinutes(1));

        // Setting IndicesOptions controls how unavailable indices are resolved and how wildcard expressions are expanded
        request.indicesOptions(IndicesOptions.strictExpandOpen());

        boolean acknowledged = false;
        try {
            OpenIndexResponse openIndexResponse = getClient().indices().open(request);
            // Indicates whether all of the nodes have acknowledged the request
            acknowledged = openIndexResponse.isAcknowledged();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return acknowledged;
    }

    @Override
    public boolean closeIndex(String indexName) {
        //The index to close
        CloseIndexRequest request = new CloseIndexRequest(indexName);

        //Timeout to wait for the all the nodes to acknowledge the index is closed as a TimeValue
        // degistirebiliriz sureyi
        request.timeout(TimeValue.timeValueMinutes(2));

        // Timeout to connect to the master node as a TimeValue
        // degistirebiliriz sureyi
        request.masterNodeTimeout(TimeValue.timeValueMinutes(1));

        // Setting IndicesOptions controls how unavailable indices are resolved and how wildcard expressions are expanded
        request.indicesOptions(IndicesOptions.lenientExpandOpen());

        boolean acknowledged = false;
        try {
            CloseIndexResponse closeIndexResponse = getClient().indices().close(request);
            // Indicates whether all of the nodes have acknowledged the request
            acknowledged = closeIndexResponse.isAcknowledged();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return acknowledged;
    }
}
