package com.bi.service.connection.impl;

import com.bi.model.connection.OdbcDriver;
import com.bi.repository.connection.OdbcDriverRepository;
import com.bi.service.connection.JdbcConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Ozgur on 20.3.2018.
 */
@Service
public class JdbcConnectionServiceImpl implements JdbcConnectionService {

    @Autowired
    OdbcDriverRepository odbcDriverRepository;

    @Override
    public void saveOdbcDriver() {
        OdbcDriver driver = new OdbcDriver();
        driver.setJdbcPreUrl("http://postgresql");
        driver.setName("PostgreSQL");
        odbcDriverRepository.save(driver);
    }
}
