package com.bi.service.connection;

import com.bi.model.connection.OdbcDriver;

/**
 * Created by Ozgur on 20.3.2018.
 */
public interface JdbcConnectionService {

    void saveOdbcDriver();
}
