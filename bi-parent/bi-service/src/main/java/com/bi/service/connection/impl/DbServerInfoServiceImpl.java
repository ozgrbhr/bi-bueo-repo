package com.bi.service.connection.impl;

import com.bi.model.connection.DbServerInfo;
import com.bi.repository.connection.DbServerInfoRepository;
import com.bi.service.connection.DbServerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DbServerInfoServiceImpl implements DbServerInfoService {

    @Autowired
    private DbServerInfoRepository dbServerInfoRepository;

    @Override
    public DbServerInfo save(DbServerInfo dbServerInfo) {
        return this.dbServerInfoRepository.save(dbServerInfo);
    }

    @Override
    public Optional<DbServerInfo> findById(Long id) {
        return this.dbServerInfoRepository.findById(id);
    }

    @Override
    public List<DbServerInfo> findAll() {
        return this.dbServerInfoRepository.findAll();
    }

    @Override
    public List<DbServerInfo> findAll(Sort sort) {
        return this.dbServerInfoRepository.findAll(sort);
    }
}
