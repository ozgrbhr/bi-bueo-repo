package com.bi.model.connection;

import javax.persistence.*;

/**
 * Created by Ozgur on 20.3.2018.
 */
@Entity
@Table(name = "db_table_info")
public class DbTableInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @ManyToOne
    private DbSchemaInfo dbSchemaInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DbSchemaInfo getDbSchemaInfo() {
        return dbSchemaInfo;
    }

    public void setDbSchemaInfo(DbSchemaInfo dbSchemaInfo) {
        this.dbSchemaInfo = dbSchemaInfo;
    }
}
