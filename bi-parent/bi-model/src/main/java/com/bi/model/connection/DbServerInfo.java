package com.bi.model.connection;

import javax.persistence.*;

/**
 * Created by Ozgur on 20.3.2018.
 */
@Entity
@Table(name = "db_server_info")
public class DbServerInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "host_name", length = 25, nullable = false)
    private String hostName;

    @Column(name = "port", length = 8, nullable = false)
    private String port;

    @Column(name = "username", length = 255, nullable = false)
    private String userName;

    @Column(name = "password", length = 255, nullable = false)
    private String password;

    @Column(name = "connection_name", length = 255, nullable = false)
    private String connectionName;

    @ManyToOne
    private OdbcDriver odbcDriver;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public OdbcDriver getOdbcDriver() {
        return odbcDriver;
    }

    public void setOdbcDriver(OdbcDriver odbcDriver) {
        this.odbcDriver = odbcDriver;
    }
}
