package com.bi.model.connection;

import javax.persistence.*;

/**
 * Created by Ozgur on 20.3.2018.
 */
@Entity
@Table(name = "db_info")
public class DbInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "char_type", length = 25, nullable = false)
    private String charType;

    @ManyToOne
    private DbServerInfo dbServerInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharType() {
        return charType;
    }

    public void setCharType(String charType) {
        this.charType = charType;
    }

    public DbServerInfo getDbServerInfo() {
        return dbServerInfo;
    }

    public void setDbServerInfo(DbServerInfo dbServerInfo) {
        this.dbServerInfo = dbServerInfo;
    }
}
