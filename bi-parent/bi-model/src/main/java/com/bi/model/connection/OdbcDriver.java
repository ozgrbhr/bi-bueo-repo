package com.bi.model.connection;

import javax.persistence.*;

/**
 * Created by Ozgur on 20.3.2018.
 */
@Entity
@Table(name = "odbc_driver")
public class OdbcDriver {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Column(name = "jdbc_pre_url", length = 50, nullable = false)
    private String jdbcPreUrl;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJdbcPreUrl() {
        return jdbcPreUrl;
    }

    public void setJdbcPreUrl(String jdbcPreUrl) {
        this.jdbcPreUrl = jdbcPreUrl;
    }
}
