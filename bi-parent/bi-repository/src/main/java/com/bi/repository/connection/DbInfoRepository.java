package com.bi.repository.connection;

import com.bi.model.connection.DbInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DbInfoRepository extends JpaRepository<DbInfo, Long> {
}
