package com.bi.repository.connection;

import com.bi.model.connection.DbServerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DbServerInfoRepository extends JpaRepository<DbServerInfo, Long> {

}
