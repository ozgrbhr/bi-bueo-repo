package com.bi.repository.connection;

import com.bi.model.connection.OdbcDriver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ozgur on 21.3.2018.
 */
@Repository
public interface OdbcDriverRepository extends JpaRepository<OdbcDriver,Long>{

}
