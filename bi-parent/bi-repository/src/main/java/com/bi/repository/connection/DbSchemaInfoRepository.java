package com.bi.repository.connection;

import com.bi.model.connection.DbSchemaInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DbSchemaInfoRepository extends JpaRepository<DbSchemaInfo, Long> {
}
