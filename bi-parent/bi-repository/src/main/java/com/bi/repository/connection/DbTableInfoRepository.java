package com.bi.repository.connection;

import com.bi.model.connection.DbTableInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DbTableInfoRepository extends JpaRepository<DbTableInfo, Long> {
}
