package com.bi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Ozgur on 12.3.2018.
 */

@SpringBootApplication
@ComponentScan(basePackages = "com.bi")
@EntityScan(basePackages = "com.bi.model")
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);


    }
}
