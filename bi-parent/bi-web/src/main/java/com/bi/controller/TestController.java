package com.bi.controller;

import com.bi.service.connection.JdbcConnectionService;
import com.bi.service.elasticsearch.ElasticsearchService;
import org.apache.http.HttpHost;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;

/**
 * Created by Ozgur on 20.3.2018.
 */
@Controller
public class TestController {

    @Autowired
    JdbcConnectionService jdbcConnectionService;

    @Autowired
    ElasticsearchService elasticsearchService;

    @GetMapping("/")
    public String getName(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
//        jdbcConnectionService.saveOdbcDriver();
        model.addAttribute("name", "Bueo BI Project");
        return "index";
    }

    @GetMapping("/es")
    public void testElasticsearch() {
        System.out.println(elasticsearchService.getClient().toString());
        elasticsearchService.closeClient();
    }
}
